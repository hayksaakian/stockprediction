#!/usr/bin/ruby 
# require 'wrap_in_module'
require_relative 'simulate_market'
require_relative 'strategy_runner'

require 'optparse'
require 'benchmark'
# gets inputs and tests
# puts '----- Stock Market Simulator -----'
# puts 'usage: ruby sm.rb strategy.rb'
# puts 'usage: ruby sm.rb strategy.rb stock_data.txt 100 false'
LOGCACHE_PATH ||= './stock_log.txt'
# DEVELOPMENT ||= true
class Sm
  attr_accessor :score
  def initialize(opts)
    strat = opts[:strategy]
    # strat = a.shift
    price_data = parse_raw_data(open(opts[:stock_data]))
    # price_data = parse_raw_data(open(a.shift))
    # d = a.any? ? a.shift.to_i : nil
    d = opts[:days]
    verbose = opts[:verbose]
    # verbose = (a.any? && a.shift.to_s == "false") ? false : true
    logging = opts[:logging]
    # logging = (a.any? && a.shift.to_s == "true") ? true : false
    filepath = logging ? LOGCACHE_PATH.gsub('_', "_#{strat}_") : nil

    @simulator = SimulateMarket.new(price_data, verbose, filepath)
    
    sr = StrategyRunner.new(strat)
    @scores = []
    while(@simulator.any_time_left? && (d.nil? ? true : d > @simulator.cd ))
      puts @simulator.net_worth_v.to_s if verbose
      puts @simulator.portfolio.to_s if verbose
      # puts Benchmark.measure {
        @input = @simulator.simulate
      # }
      puts @input if verbose
      # puts Benchmark.measure {
      #   @output = `ruby #{strat} #{filepath}`
      # }
      # puts Benchmark.measure {
        # @output = `echo "#{@input}" | ruby #{strat}`
      # }
      # puts Benchmark.measure {
        @output = sr.run(@input)
      # }
      # raise Error
      # puts output
      @scores.push(@simulator.score)
      @simulator.take_orders(@output)
    end
    # @score = @simulator.score
    if opts[:array_of_scores]
      puts @scores.to_s.gsub('[', '').gsub(']', '') 
    else
      puts @simulator.score
    end
    # o = `echo "#{scores.to_s.gsub('[', '').gsub(']', '')}" | spark` if verbose
    # puts @score
  end

  def parse_raw_data(s_data)
    # puts 'parsing '+s_data.size.to_s+' bytes'
    price_data = {}
    while (l=s_data.gets)
      a = l.split
      name = a[0]
      prices = a[1..a.length].map{|n| n.to_f}
      price_data[name] = prices
    end
    # puts @price_data.to_s
    return price_data
  end
end