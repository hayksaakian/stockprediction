#!/usr/bin/ruby 
CASH_STOCK_RATIO ||= 0.20
DEVELOPMENT ||= false
class Strategy
  # utility functions
  def deep_copy(o)
    Marshal.load(Marshal.dump(o))
  end
  # math things
  class SimpleLinearRegression
    def initialize(xs, ys)
      @xs, @ys = xs, ys
      if @xs.length != @ys.length
        raise "Unbalanced data. xs need to be same length as ys"
      end
      @cached_slope = nil
    end

    def y_intercept
      mean(@ys) - (slope * mean(@xs))
    end

    def slope
      unless @cached_slope
        x_mean = mean(@xs)
        y_mean = mean(@ys)
        numerator = (0...@xs.length).reduce(0) do |sum, i|
          sum + ((@xs[i] - x_mean) * (@ys[i] - y_mean))
        end
        denominator = @xs.reduce(0) do |sum, x|
          sum + ((x - x_mean) ** 2)
        end
        @cached_slope = (numerator / denominator)
      end
      return @cached_slope
    end

    def mean(values)
      total = values.reduce(0) { |sum, x| x + sum }
      Float(total) / Float(values.length)
    end
  end
  def average(arr)
    return arr.reduce(:+) / arr.length
  end
  def sqrt(n)
    return n**(0.5)
  end
  def variance(arr)
    avg = average(arr)
    diffs = arr.map{|x| x - avg }
    ttl_sq_diffs = diffs.reduce(0) {|mmo,val| mmo += (val*val)}
    return ttl_sq_diffs / arr.length
  end
  def stdev(arr)
    sqrt(variance(arr))
  end
  def slope(arr)
    xs = 0.upto(arr.length - 1).to_a
    lr = SimpleLinearRegression.new(xs , arr)
    return lr.slope
  end

### Head ends here
  # m - the amount of money you could spend that day.
  # k - the number of different stocks available for buying or selling.
  # d - the number of remaining days for trading stocks.
  def printTransactions(money, k, d, names, owned, prices)
    @d = d
    # puts money
    # puts money, k, d, names, owned, prices
    @money = money
    # strategy rules
    # keep half of your assets as cash, and half as stocks
    @cash_stock_ratio = CASH_STOCK_RATIO
    # note: spending money 'm' decreases from BUY
    # but does not increase from SELL until the next day
    
    @portfolio = {}
    def net_worth(the_portfolio=nil)
      the_portfolio = @portfolio if the_portfolio.nil?
      return @money + assets_worth(the_portfolio)
    end
    def assets_worth(portfolio)
      return portfolio.reduce(0) do |v, (nm, s)| 
        v += (s['quantity'].to_f*s['prices'][-1])
      end
    end
    def structure_and_analyze(money, k, d, names, owned, prices)
      def figure_of_merit(stock)

      end
      0.upto(k-1) do |i|
        # structure data
        stock = {}
        stock['name'] = names[i]
        stock['quantity'] = owned[i]
        stock['prices'] = prices[i]
        stock['price'] = prices[i].last
        
        # analyze
        stock['stdev'] = stdev(stock['prices'])
        stock['slope'] = slope(stock['prices'])
        
        # score
        stock['rating'] = stock['slope']

        @portfolio[stock['name']] = stock
      end
      ranking_criteria = ['slope', 'stdev']
      low_is_good = ['slope', 'stdev'] # good to buy that is?
      # rankings will be best to worst, 1 ... 10
      rankings = {}
      ranking_criteria.each do |cr|
        rankings[cr] = @portfolio.values.sort_by{|s| s[cr]}
      end
      # Kernel.puts rankings['slope']
      def indexof(arr, name)
        arr.index(arr.find{|s| s['name'] == name})
      end
      # Kernel.puts indexof(rankings['slope'], 'UCLA')
      # raise 'PAUSE'
      stdev_rankings = @portfolio.values.sort_by{|s| s['stdev']}

      @portfolio.each do |sname, stock|
        ranks = {}
        ranking_criteria.each do |cr|
          ranks[cr] = indexof(rankings[cr], sname)
        end
        @portfolio[sname]['ranks'] = ranks
      end
    end
    structure_and_analyze(money, k, d, names, owned, prices)

    @orders = {}
    def buy(stock, quantity=1)
      # puts stock
      place_order('BUY', stock, quantity)
    end
    def sell(stock, quantity=1)
      # puts stock
      place_order('SELL', stock, quantity)
    end
    # normal .clone is not deep
    @future_portfolio = deep_copy(@portfolio)
    def place_order(buy_or_sell, stock, quantity)
      # puts stock.to_s
      rq = buy_or_sell == 'SELL' ? (-1*quantity) : quantity # make sure sells are a negative buy
      order = @orders[stock['name']] || {}
      order['name'] ||= stock['name']
      order['quantity'] = order.include?('quantity') ? order['quantity']+rq : rq
      if order['quantity'] != 0 
        order['kind'] = order['quantity'] > 0 ? 'BUY' : 'SELL'
      end
      order['quantity'] = order['quantity'].abs
      @orders[stock['name']] = order

      if order['quantity'] == 0
        @orders.delete(stock['name'])
      end
      @future_portfolio[stock['name']]['quantity'] = @future_portfolio[stock['name']]['quantity'] + rq.to_f
    end
    # rough test 
    # goes from small to large
    stocks_by_rating = @portfolio.values.sort_by{|s| s['slope']}
    
    # Determine buys
    @bmoney = money
    buy_passes = 0
    def try_buys(stocks, bmoney)
      stocks.each do |stock|
        #criteria for buying a stock:
        if bmoney >= @cash_stock_ratio * net_worth
          q = 1.0
          cost = q.to_f*stock['price']
          # only buy if you can afford it
          if bmoney > cost
            # puts 'trying to buy '+q.to_s+' of '+stock['name']
            buy(stock, q)
            bmoney -= cost
          end
        end
      end
      return bmoney
    end
    while(buy_passes < 3 && (@bmoney >= @cash_stock_ratio * net_worth)) do
      @bmoney = try_buys(stocks_by_rating, @bmoney)
      buy_passes += 1
    end

    # Determine sells
    sell_passes = 0
    def supposed_assets_value 
      return assets_worth(@future_portfolio) + @bmoney
    end
    reversed_stocks = stocks_by_rating.reverse
    def try_sells(stocks, bmoney)
      stocks.each do |stock|
        # sell strategy
        # need to actually own the stock to sell it
        if(stock['quantity'] > 0 && bmoney < (@cash_stock_ratio * (assets_worth(@future_portfolio) + bmoney)))
          # puts 'trying to sell '+stock['quantity'].to_s+' of '+stock['name']
          # Kernel.puts @k
          # q = stock['quantity'] # instead of selling only 1, sell all
          q = 1.0
          sell(stock, q)
          bmoney += (q.to_f*stock['price'])
        end
      end
      return bmoney
    end

    while (sell_passes < 3 && (@cash_stock_ratio * (assets_worth(@future_portfolio) + @bmoney)))
      @bmoney = try_sells(reversed_stocks, @bmoney)
      sell_passes += 1
    end

    def submit_orders(orders)
      puts @orders.length
      @orders.each do |i, ordr|
        # puts i
        # puts ordr
        ordr['submitted'] = true
        puts "#{ordr['name']} #{ordr['kind']} #{ordr['quantity'].to_i.to_s}"
      end
      return @orders
    end

    submit_orders(@orders)
  end

  def read_stdin(gs)
    @gs = gs
    def gets
      @gs.call
    end
    # Tail starts here
    m,k,d = gets.strip.split.map! {|data| data.to_f}
    k = k.to_i
    d = d.to_i
    the_names = []
    owned = []
    prices = []
    1.upto(k) do |inp|
      temp = gets.strip.split
      the_names << temp[0]
      owned << temp[1].to_i
      prices << temp[2..6].map { |data| data.to_f}
    end
    # puts the_names
    printTransactions(m, k, d, the_names, owned, prices)
  end
end
if DEVELOPMENT
  @f = File.open(LOGCACHE_PATH)
  @linecache = []
  @numgets = 0
  while line = @f.gets do
    # puts line
    @linecache.push(line.strip)
  end
  # puts @linecache
end
def dogets
  # Kernel.puts 'super gets'
  if defined?(@f) && @numgets < @linecache.length
    @numgets += 1
    r = @linecache[@numgets]
    # puts r
    return r
  else
    # puts 'normal gets'
    # r = Kernel.gets
    # puts r
    # return r
    Kernel.gets
  end
end
s = Strategy.new
s.read_stdin(->(){dogets})
if DEVELOPMENT
  @f.close
end