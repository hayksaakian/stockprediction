# require_relative 'sm'
# TODO: make this more like rsm.rb
# usage
# ruby smc.rb strategy1.rb strategy2.rb stock_data.text
class Smc
	def initialize(args)
		a = args.shift.to_s
		b = args.shift.to_s
		data = args.shift.to_s
		days = args.shift.to_i || 499
		verbose = (args.shift.to_s != "true") ? false : true

		
		puts "Starting comparison" if verbose
		t = Time.now
		outputa = `ruby rsm.rb #{a} #{data} #{"-v" if verbose}`
		puts "finished #{a} scored #{outputa.to_s.chomp} took #{(Time.now-t).to_s}" if !verbose
		t = Time.now
		outputb = `ruby rsm.rb #{b} #{data} #{"-v" if verbose}`
		puts "finished #{b} scored #{outputb.to_s.chomp} took #{(Time.now-t).to_s}" if !verbose
		
		puts "finished #{a} scored #{outputa.to_s}" if verbose
		puts "finished #{b} scored #{outputb.to_s}" if verbose
		
	end
end
Smc.new(ARGV)