#!/usr/bin/env ruby
require_relative 'sm'
def read_options
  require 'optparse'

  options = {}
  options[:verbose] = false
  options[:days] = 500
  OptionParser.new do |opts|
    opts.banner = "Usage: ./rsm.rb strategy.rb stock_data.txt [options]"
    
    # Cast 'delay' argument to a Float.
    opts.on("-d", "--days N", Integer, "Simulate N days of trading") do |n|
      options[:days] = n
    end
    opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
      options[:verbose] = v
    end
    opts.on("-l", "--[no-]logging", "Log the last output") do |l|
      options[:logging] = l
    end
    opts.on("-a", "--[no-]array_of_scores", "Keep a list of all scores") do |a|
      options[:array_of_scores] = a
    end
  end.parse!
  return options
end
options = read_options

options[:strategy] = ARGV.shift
options[:stock_data] = ARGV.shift

sm = Sm.new(options)

# puts sm.score