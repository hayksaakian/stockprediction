#!/usr/bin/ruby 
# rate/arrange stocks based on their change in the past 1 day
WEIGHTS = {
  'second_slope' => 0.0,
  'concavity' => -2.5,
  'delta' => -5.0, # price drop == good (meaning negative delta)
  'slope' => 0.0,
  'stdev' => 0.0,
  'market_correlation' => 0.0
}
# 47.671726062959635 - no concavity, just prev day and today
# 48.19571188679948 - added multiple buying / selling
# 48.19571188679948 - fixed comparison in while loop bug
# 48.062005124153764 - with arbitrary trade limit
# ARBITRARY_LIMIT ||= 4
# 49.067193289330824 - scale ratio based on volatility
# @cash_stock_ratio = CASH_STOCK_RATIO + (CASH_STOCK_RATIO*0.25*ms)
# 49.196723432250536 fixed bug with stocks not netting correctly
# 49.61779385604697 now with a figure of merit thing

CASH_STOCK_RATIO ||= 0.20
DEVELOPMENT ||= false
class Strategy
  # utility functions
  def deep_copy(o)
    Marshal.load(Marshal.dump(o))
  end
  # math things
  class SimpleLinearRegression
    def initialize(xs, ys)
      @xs, @ys = xs, ys
      if @xs.length != @ys.length
        raise "Unbalanced data. xs need to be same length as ys"
      end
      @cached_slope = nil
    end

    def y_intercept
      mean(@ys) - (slope * mean(@xs))
    end

    def slope
      unless @cached_slope
        x_mean = mean(@xs)
        y_mean = mean(@ys)
        numerator = (0...@xs.length).reduce(0) do |sum, i|
          sum + ((@xs[i] - x_mean) * (@ys[i] - y_mean))
        end
        denominator = @xs.reduce(0) do |sum, x|
          sum + ((x - x_mean) ** 2)
        end
        @cached_slope = (numerator / denominator)
      end
      return @cached_slope
    end

    def mean(values)
      total = values.reduce(0) { |sum, x| x + sum }
      Float(total) / Float(values.length)
    end
  end
  # other math functions
  #dat calculus
  def deltas(arr)
    arr.each_cons(2).map{|x, y| y - x}
  end
  def increasing(arr)
    return slope(arr) > 0
  end
  def decreasing(arr)
    return slope(arr) < 0
  end
  def concave_up(arr)
    return increasing(deltas(arr)) == true
  end
  def concave_down(arr)
    return concave_up(arr) == false
  end
  def concavity(arr)
    concave_up(arr) ? 1 : -1
  end
  def mean(arr)
    total = arr.reduce(0) { |sum, x| x + sum }
    return Float(total) / Float(arr.length)
  end
  def avg_up(arr)
    return increasing(arr) == true
  end
  def avg_down(arr)
    return avg_up(arr) == false
  end
  
  def average(arr)
    return arr.reduce(:+) / arr.length
  end
  def sqrt(n)
    return n**(0.5)
  end
  def variance(arr)
    avg = average(arr)
    diffs = arr.map{|x| x - avg }
    ttl_sq_diffs = diffs.reduce(0) {|mmo,val| mmo += (val*val)}
    return ttl_sq_diffs / arr.length
  end
  def stdev(arr)
    sqrt(variance(arr))
  end
  def slope(arr)
    xs = 0.upto(arr.length - 1).to_a
    lr = SimpleLinearRegression.new(xs , arr)
    return lr.slope
  end

  # 1 is a perfect positive correlation
  # 0 is no correlation (the values don't seem linked at all)
  # -1 is a perfect negative correlation
  def correlation(arrA, arrB)
    if arrA.length != arrB.length
      raise "Cannot correlate arrays of different length"
    end
    avgA = average(arrA)
    avgB = average(arrB)
    a = arrA.map{|x| avgA - x}
    b = arrB.map{|x| avgB - x}
    ab = []
    asquared = []
    bsquared = []
    a.each_with_index do |ai, i|
      ab.push(ai*b[i])
      asquared.push(ai*ai)
      bsquared.push(b[i]*b[i])
    end
    ab_total = ab.reduce(:+)
    asquared_total = asquared.reduce(:+)
    bsquared_total = bsquared.reduce(:+)
    cor = ab_total / sqrt(asquared_total*bsquared_total)
    return cor
  end

### Head ends here
  # m - the amount of money you could spend that day.
  # k - the number of different stocks available for buying or selling.
  # d - the number of remaining days for trading stocks.
  def printTransactions(money, k, d, names, owned, prices)
    @d = d
    # puts money
    # puts money, k, d, names, owned, prices
    @money = money
    # strategy rules
    # keep half of your assets as cash, and half as stocks
    @cash_stock_ratio = CASH_STOCK_RATIO

    # new strategy:
    # change cash_stock_ratio based on how market is doing


    @portfolio = {}
    def net_worth(the_portfolio=nil)
      the_portfolio = @portfolio if the_portfolio.nil?
      return @money + assets_worth(the_portfolio)
    end
    def assets_worth(portfolio)
      return portfolio.reduce(0) do |v, (nm, s)| 
        v += (s['quantity'].to_f*s['prices'][-1])
      end
    end
    def market_slope
      avg_prices = []
      numdays = @portfolio.values.first['prices'].length
      (0..(numdays-1)).each do |i|
        avg_prices[i] = 0
        @portfolio.each do |n, stock|
          avg_prices[i] += stock['prices'][i]
        end
        avg_prices[i] = avg_prices[i] / numdays.to_f
      end
      slope(avg_prices)
    end

    def structure_and_analyze(money, k, d, names, owned, prices)
      # keep in mind that low to high is good
      # the value of the attribute of each stock should be normalized
      # 1 being good
      # 0 being whatever
      # -1 being bad
      def figure_of_merit(stock)
        fom = 0 # or maybe stock['ranking']
        WEIGHTS.each do |k, vl|
          fom += stock[k] * vl
        end
        return fom
      end
      0.upto(k-1) do |i|
        # structure data
        stock = {}
        stock['name'] = names[i]
        stock['quantity'] = owned[i]
        stock['prices'] = prices[i]
        stock['price'] = prices[i].last
        # analyze
        stock['stdev'] = stdev(stock['prices'])
        stock['slope'] = slope(stock['prices'])
        stock['concavity'] = concavity(stock['prices'])
        stock['second_slope'] = slope(deltas(stock['prices']))
        # stock['increasing'] = increasing(stock['prices'])
        stock['ath'] = stock['prices'].max
        stock['atl'] = stock['prices'].min
        # score/rate the stock
        stock['delta'] = stock['prices'][-1]-stock['prices'][-2]
        stock['rating'] = stock['prices'][-1]-stock['prices'][-2]
        @portfolio[stock['name']] = stock
      end
      @ms = market_slope
      # calculate daily totals in order to eventually draw the average
      numdays = @portfolio.values.first['prices'].length
      daily_totals = []
      0.upto(numdays-1) do |i|
        daily_totals[i] ||= 0
        @portfolio.values.each do |stock|
          daily_totals[i] += stock['prices'][i]
        end
      end
      # calculate daily averages
      daily_averages = daily_totals.map{|dt| dt/@portfolio.length}
      # determine market correlation
      @portfolio.each do |nm, stock|
        mc = correlation(daily_averages, stock['prices'])
        @portfolio[nm]['market_correlation'] = mc
      end
      def indexof(arr, name)
        arr.index(arr.find{|s| s['name'] == name})
      end
      # Kernel.puts indexof(rankings['slope'], 'UCLA')
      # raise 'PAUSE'
      @portfolio.each do |sname, stock|
        @portfolio[sname]['fom'] = figure_of_merit(stock)
      end
    end
    structure_and_analyze(money, k, d, names, owned, prices)

    # when the market is tanking, i want more of my net in cash
    # when it's rising, i want more of my net in stocks
    ms = market_slope
    # Kernel.puts "Market Slope: #{ms.to_s}"
    @cash_stock_ratio = CASH_STOCK_RATIO + (CASH_STOCK_RATIO*0.35*ms)

    @orders = {}
    def buy(stock, quantity=1)
      # puts stock
      # puts quantity
      place_order('BUY', stock, quantity)
    end
    def sell(stock, quantity=1)
      # puts stock
      # puts quantity
      place_order('SELL', stock, quantity)
    end
    # normal .clone is not deep
    @future_portfolio = deep_copy(@portfolio)
    def place_order(buy_or_sell, stock, quantity)
      # puts stock.to_s
      # rq = (buy_or_sell == 'SELL') ? (-1*quantity) : quantity # make sure sells are a negative buy
      order = {}
      order = @orders[stock['name']] || {}
      order['name'] ||= stock['name']
      if order.include?('quantity')
        if order['kind'] == buy_or_sell
          order['quantity'] += quantity
        else
          order['quantity'] -= quantity
          order['quantity'] = order['quantity'].abs
          if buy_or_sell == 'SELL'
            if order['quantity'] < 0
              order['kind'] = buy_or_sell
            end
          elsif buy_or_sell == 'BUY'
            if order['quantity'] > 0
              order['kind'] = buy_or_sell
            end           
          end
        end
      else
        order['quantity'] = quantity
        order['kind'] = buy_or_sell
      end

      order['mathematical_quantity'] = (order['kind'] == 'BUY' ? 1 : -1) * order['quantity']

      @orders[stock['name']] = order

      future_q = @portfolio[stock['name']]['quantity'] + order['mathematical_quantity']

      if order['quantity'] == 0
        @orders.delete(stock['name'])
      end

      @future_portfolio[stock['name']]['quantity'] = future_q
    end
    # rough test 
    # goes from small to large
    # stocks_by_rating = @portfolio.values.sort_by{|s| s['slope']}
    # arrange stocks based on their change in the past 1 day
    # score = last price - second to last price
    # -120 = 200 - 80 # just rose a lot == good stock
    # reverse in order to sort from large to small
    stocks_by_rating = @portfolio.values.sort_by{|s| s['fom']}.reverse
    
    # Determine buys
    @bmoney = money
    buy_passes = 0
    # @numbuys = 0
    
    def try_buys(stocks, bmoney)
      stocks.each do |stock|
        #criteria for buying any stock:
        if bmoney >= (@cash_stock_ratio * net_worth(@future_portfolio))
          q = 1.0
          cost = q.to_f*stock['price']
          if concave_down(stock['prices']) && decreasing(stock['prices'])
            # buy if a stock dove, but seems to be picking up
            nq = q + 1.0
            nc = nq.to_f*stock['price']
            if bmoney > nc
              q = nq
              cost = nc
            end
          end
          # only buy if you can afford it
          # this does not seem to work sometimes
          if bmoney > cost
            # raise 'trying to buy '+q.to_s+' of '+stock['name']
            # @numbuys += 1
            # break if @numbuys > ARBITRARY_LIMIT
            buy(stock, q)
            bmoney -= cost
          end
        end
      end
      return bmoney
    end
    while(buy_passes < 10 && (@bmoney >= @cash_stock_ratio * net_worth(@future_portfolio))) do
      @bmoney = try_buys(stocks_by_rating, @bmoney)
      buy_passes += 1
    end
    # puts @portfolio.values
    # puts @orders.values
    # Determine sells
    sell_passes = 0
    def supposed_assets_value 
      return assets_worth(@future_portfolio) + @bmoney
    end
    reversed_stocks = stocks_by_rating.reverse
    def try_sells(stocks, bmoney)
      stocks.each do |stock|
        # sell strategy
        # need to actually own the stock to sell it
        sq = stock['quantity']
        if @orders.include?(stock['name'])
          anyo = nil
          anyo ||= @orders[stock['name']]
          if !anyo.nil? && anyo['kind'] == 'SELL'
            # puts "gotem #{stock['name']}"
            # puts sq
            sq = sq - anyo['quantity']
            # puts sq
          end
        end
        if(sq > 0 && bmoney < (@cash_stock_ratio * (assets_worth(@future_portfolio) + bmoney)))
          # puts 'trying to sell '+stock['quantity'].to_s+' of '+stock['name']
          # Kernel.puts @k
          # q = stock['quantity'] # instead of selling only 1, sell all
          q = 1.0
          if concave_down(stock['prices']) && !increasing(stock['prices'])
            # sell if a stock looks like it's about to dive
            if q < 0.49*sq
              q += 1.0
            end
          end
          # Kernel.puts 'trying to sell '+q.to_s+' of '+stock['name']
          sell(stock, q)

          bmoney += (q.to_f*stock['price'])
        end
      end
      return bmoney
    end

    while (sell_passes < 10 && @bmoney < (@cash_stock_ratio * (assets_worth(@future_portfolio) + @bmoney)))
      @bmoney = try_sells(reversed_stocks, @bmoney)
      sell_passes += 1
    end

    def submit_orders(orders)
      puts @orders.length
      @orders.each do |i, ordr|
        # puts i
        # puts ordr
        ordr['submitted'] = true
        puts "#{ordr['name']} #{ordr['kind']} #{ordr['quantity'].to_i.to_s}"
      end
      return @orders
    end

    submit_orders(@orders)
  end

  def read_stdin(gs)
    @gs = gs
    def gets
      @gs.call
    end
    # Tail starts here
    m,k,d = gets.strip.split.map! {|data| data.to_f}
    k = k.to_i
    d = d.to_i
    the_names = []
    owned = []
    prices = []
    1.upto(k) do |inp|
      temp = gets.strip.split
      the_names << temp[0]
      owned << temp[1].to_i
      prices << temp[2..6].map { |data| data.to_f}
    end
    # puts the_names
    printTransactions(m, k, d, the_names, owned, prices)
  end
end
if DEVELOPMENT
  @f = File.open(LOGCACHE_PATH)
  @linecache = []
  @numgets = 0
  while line = @f.gets do
    # puts line
    @linecache.push(line.strip)
  end
  # puts @linecache
end
def dogets
  # Kernel.puts 'super gets'
  if defined?(@f) && @numgets < @linecache.length
    @numgets += 1
    r = @linecache[@numgets]
    # puts r
    return r
  else
    # puts 'normal gets'
    # r = Kernel.gets
    # puts r
    # return r
    Kernel.gets
  end
end
s = Strategy.new
s.read_stdin(->(){dogets})
if DEVELOPMENT
  @f.close
end