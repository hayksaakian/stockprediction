#!/usr/bin/ruby 
# 48.07131812685189

# Head ends here
def printTransactions(money, kinds_available, days_left, name, owned, prices)
  stocks = []
  my_stocks = []
  unowned_stocks = []
  transactions = []
  # make OO format
  name.each_with_index do |nm, i|
    stock = {}
    stock[:name] = nm
    stock[:owned] = owned[i]
    stock[:prices] = prices[i]
    stocks.push(stock)
    if stock[:owned] > 0
      my_stocks.push(stock)
    else
      unowned_stocks.push(stock)
    end
  end
  
  #dat calculus
  def deltas(arr)
    arr.each_cons(2).map{|x, y| y - x}
  end
  def increasing(arr)
    return slope(arr) > 0
  end
  def concave_up(arr)
    return increasing(deltas(arr)) == true
  end
  def concave_down(arr)
    return concave_up(arr) == false
  end
  
  def mean(arr)
    total = arr.reduce(0) { |sum, x| x + sum }
    return Float(total) / Float(arr.length)
  end

  def slope(arr)
    xs = (1..arr.length).to_a
    ys = arr
    x_mean = mean(xs)
    y_mean = mean(ys)
   
    numerator = (0...xs.length).reduce(0) do |sum, i|
      sum + ((xs[i] - x_mean) * (ys[i] - y_mean))
    end
   
    denominator = xs.reduce(0) do |sum, x|
      sum + ((x - x_mean) ** 2)
    end
   
    return (numerator / denominator)
  end

  def avg_up(arr)
    return increasing(arr) == true
  end
  
  def avg_down(arr)
    return avg_up(arr) == false
  end
  
  def buy(name, quantity)
    return name+' BUY '+quantity.to_s
  end
  
  def sell(name, quantity)
    return name+' SELL '+quantity.to_s
  end
  
  #determine sells
  my_stocks.each do |s|
    if concave_up(s[:prices]) && avg_up(s[:prices])
      quantity = 1
      transactions.push(sell(s[:name], quantity))
    end
  end
  # determine buys
  # instead of random order, consider sorting by how 'hot' each stock is
  hot_stocks = stocks.sort_by {|st| (slope(deltas(st[:prices])))}
  hot_stocks.each do |s|
    if avg_down(s[:prices]) && concave_down(s[:prices])
      quantity = 1
      if s[:prices].last <= money
        transactions.push(buy(s[:name], quantity))
        money -= s[:prices].last
      end
    end
  end
  puts transactions.length
  transactions.each do |t|
    puts t
  end
end

# Tail starts here
m,k,d = gets.strip.split.map! {|data| data.to_f}
k = k.to_i
d = d.to_i
names = []
owned = []
prices = []
1.upto(k) do |inp|
  temp = gets.strip.split
  names << temp[0]
  owned << temp[1].to_i
  prices << temp[2..6].map { |data| data.to_f}
end

printTransactions(m, k, d, names, owned, prices)