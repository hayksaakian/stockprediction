#!/usr/bin/ruby 

class SimulateMarket
  attr_accessor :cd
  # @cash_on_hand
  # @stocks
  # @d
  # @d_max
  # @cd
  # @std_cache

  # @verbose

  QPD = 5
  # @cachefilepath
  def initialize(price_data, verbose=true, cachefilepath=nil)
    @verbose = verbose
    @cachefilepath = cachefilepath

    @cash_on_hand = 100.00
    @cd = 0
    @price_data = price_data
    
    @dl = price_data.values.first.length
    @d = @dl - QPD
    @d_max = @d
    
    @stocks = {}
    price_data.each do |n, prices|
      vn = {}
      vn['name'] = n
      vn['owned'] = 0
      vn['prices'] = prices
      @stocks[n] = vn
    end
  end
  def simulate
    inputs = []
    # present input data
    # m k d
    inputs.push "#{@cash_on_hand.to_s} #{@stocks.length.to_s} #{@d}"
    @stocks.each do |n, stock|
      inputs.push "#{n} #{stock['owned'].to_s} #{stock['prices'][(@cd..(@cd+QPD-1))].join(' ')}"
    end
    @lastinput = inputs.join("\n")

    unless @cachefilepath.nil?
      File.open(@cachefilepath, 'w') do |f2|  
        f2.puts @lastinput 
      end
    end

    @cd += 1
    @d -= 1
    return @lastinput
  end
  def take_orders(output)
    lines = output.split("\n")
    num = lines.shift.to_i
    buyqueue = {}
    sellqueue = {}
    num.times do |i|
      o = lines.shift
      parts = o.split(' ')
      stockname = parts.shift
      orderkind = parts.shift
      quantity = parts.shift.to_f
      raise "Error #{stockname} was not found in the list stocks" unless @stocks.keys.include?(stockname)
      raise "Error #{orderkind} is an invalid order, only BUY or SELL are allowed" unless['BUY', 'SELL'].include?(orderkind)
      buyqueue[stockname] = quantity if orderkind == 'BUY'
      sellqueue[stockname] = quantity if orderkind == 'SELL'
    end
    
    theday = @cd+QPD-2
    # Kernel.puts theday
    # raise Error
    buyqueue.each do |name, quantity|
      q = quantity.to_f
      pr = @stocks[name]['prices'][theday]
      price = q * pr
      if price <= @cash_on_hand
        Kernel.puts "Buying #{q.to_s} of #{name} for #{price.to_s}" if @verbose
        # buy
        @stocks[name]['owned'] += quantity
        @cash_on_hand -= price
      else
        e = "Not enough Cash to order stocks $#{price.to_s} > $#{@cash_on_hand.to_s}"
        e << " Buying #{q.to_s} of #{name} for #{price.to_s}"
        raise e
      end
    end
    sellqueue.each do |name, quantity|
      q = quantity.to_f
      pr = @stocks[name]['prices'][theday]
      price = q * pr
      if quantity <= @stocks[name]['owned']
        Kernel.puts "Selling #{q.to_s} of #{name} for #{price.to_s}" if @verbose
        # buy
        @stocks[name]['owned'] -= quantity
        @cash_on_hand += price
      else
        raise "Not enough #{name} Stocks. You tried to sell #{quantity.to_s}; you only have #{@stocks[name]['owned']}"
      end
    end
  end
  def assets_worth
    w = 0
    @stocks.each do |k, v|
      w += v['owned'].to_f*v['prices'][@cd+QPD-2]
    end
    return w
  end
  def score
    # log is ln in this case
    return 5.0 * Math.log(net_worth)
  end
  def net_worth
    return @cash_on_hand + assets_worth
  end
  def market_value
    w = 0
    @stocks.each do |k, v|
      w += v['prices'][@cd+QPD-2]
    end
    return w
  end
  def net_worth_v
    return {
      'Day' => @cd.round(2),
      'Net Worth' => net_worth.round(2),
      'Score' => score.round(2),
      'Cash on Hand' => @cash_on_hand.round(2),
      'Value of Stock' => assets_worth.round(2),
      'Market Value' => market_value.round(2)
    }
  end
  def any_time_left?
    return @d > 0
  end
  def portfolio
    pf = {}
    @stocks.each do |sname, s|
      # puts s
      pf[s['name']] = s['owned'] if s['owned'] > 0
    end
    return pf
  end
end