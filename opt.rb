def read_options
	require 'optparse'

	options = {}
	options[:verbose] = false
	options[:days] = 500
	OptionParser.new do |opts|
	  opts.banner = "Usage: sm.rb [options]"
		
		# Cast 'delay' argument to a Float.
	  opts.on("-d", "--days N", Integer, "Simulate N days of trading") do |n|
	    options[:days] = n
	  end
	  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
	    options[:verbose] = v
	  end
	  opts.on("-l", "--[no-]logging", "Log the last output") do |l|
	    options[:logging] = l
	  end
	end.parse!
	return options
end
puts read_options