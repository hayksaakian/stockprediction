require "stringio"
module Kernel
  def capture_stdout
    out = StringIO.new
    $stdout = out
    yield
    return out
  ensure
    $stdout = STDOUT
  end
end
class StrategyRunner
  def initialize(strategy_file)
    @strategy = strategy_file
  end
  def run_with_file(input_file, strategy_file=nil)
    f = File.read(input_file)
    return self.run(f, strategy_file)
  end
  def run(input_string, strategy_file=nil)
    strategy_file ||= @strategy
    return StrategyRunner.fastrun(input_string, strategy_file)
  end
  def self.fastrun(input_string, strategy_file)
    # $stdin.flush
    # clear it so it doesn't mess up
    # any .gets that get called
    ARGV.clear
    sin = StringIO.new(input_string)
    # sin.
    $stdin = sin
    # via https://www.ruby-forum.com/topic/66012
    stdout = Kernel.capture_stdout do
      load strategy_file, true
    end
    # $stdout = STDOUT
    $stdin = STDIN
    stdout.rewind
    output = stdout.read

    return output
  end
end
# t = StrategyRunner.new(ARGV[0])
# # t = StrategyRunner.new('tradebycalc.rb')
# puts t.run_with_file('stock_log.txt')